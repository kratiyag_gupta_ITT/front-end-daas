function drawCircle(radius) {
    var canvas = document.getElementById("topCanvas");
    var context = canvas.getContext("2d");
    context.beginPath();
    context.arc(canvas.width / 2, canvas.height / 2, radius, 0, 2 * Math.PI);
    context.stroke();
}

function drawRect(length, width) {
    var canvas = document.getElementById("topCanvas");
    var context = canvas.getContext("2d");
    context.beginPath();
    context.rect(10, 10, length, width);
    context.stroke();
}

function isvalidateInput(choice, radius, length, width) {
    var canvas = document.getElementById("topCanvas");
    var messageNode = document.getElementById("errorMessage");
    if ((choice == 1) &&  (radius > canvas.width / 2 || radius > canvas.height / 2)) {
        messageNode.innerHTML = "Please enter radius less than " + canvas.height / 2;
        messageNode.style.color = "red";
        return false;
    }
    if((choice == 2) && (length > canvas.length - 10 || width > canvas.width - 10))  {
        messageNode.innerHTML = "Please enter length and width less than " + canvas.height / 2 +"&" +canvas.width;
        messageNode.style.color = "red";
        return false;
    }
    return true;
}

function clearCanvas() {
    var canvas = document.getElementById("topCanvas");
    var context = canvas.getContext("2d");
    context.clearRect(0, 0, canvas.width, canvas.height);
}

function drawShapes() {
    var selectBar = document.getElementById("selectShapes");
    var selectShape = selectBar.value;
    var radius = document.getElementById("circleRadius").value;
    var length = document.getElementById("length").value;
    var width = document.getElementById("width").value;
    if (!isvalidateInput(selectShape, radius, length, width)) {
        document.getElementById("errorMessage").innerHTML = "Invalid Input use ','  in order to differencite between width and height";
    }
    else if(selectShape == 1){
        drawCircle(radius);
    }
    else {
        drawRect(length, width);
    }
}

function selectShapes() {
    var selectBar = document.getElementById("selectShapes");
    var selectShape = selectBar.value;
    if(selectShape == 1) {
            document.getElementById("userMessage").innerHTML = "Enter Radius:- ";
            document.getElementById("circleRadius").style.display="";
            document.getElementById("rectangleInput").style.display="none";
    }
    else{
            document.getElementById("userMessage").innerHTML = "Enter length and width respectively:- ";
            document.getElementById("circleRadius").style.display="none";
            document.getElementById("rectangleInput").style.display="";
    }
}