function applyStyle(elementId) {
    
    var flexContainer = document.getElementById("flexContainer");
    if(elementId=="rowAlign"){
        flexContainer.style.flexFlow="row";
    }
    else if(elementId=="colAlign") {
        flexContainer.style.flexFlow="column";
    }
    else if(elementId=="reverse") {
        flexContainer.style.flexFlow="row-reverse"; 
    }
    else if(elementId=="centerAlign") {
        flexContainer.style.flexFlow="row";
        flexContainer.style.justifyContent="center";
    }
    else if(elementId=="alignFlexStart") {
        flexContainer.style.justifyContent="flex-start";
    }
    else if(elementId=="alignFlexEnd") {
        flexContainer.style.justifyContent="flex-end";
    }
}